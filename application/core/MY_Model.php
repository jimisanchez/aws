<?php

class MY_Model extends CI_Model {

    protected $_pk = 'id';
    protected $active = 'active';
    protected $table_name;
    protected $limit;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @tutorial This method create and update table patient
     * @param array $fields is an array with all fields I want to update
     * @param string $pk primary key name (default id)
     * @return boolean
     */
    function save($fields, $pk = NULL) {
        if (is_null($pk)) {
            $pk = $this->_pk;
        }

        //  If primary key exist in fields array, then it is insert, but it is update
        if (!isset($fields[$pk])) {
            $this->db->set($fields);
            $this->db->insert($this->table_name);

            if ($this->db->affected_rows() != 1) {
                if ($error = $this->db->_error_message()) {
                    log_message('error', $error);
                }
                return FALSE;
            } else {
                return $this->db->insert_id();
            }
        } else {
            $this->db->where($pk, $fields[$pk]);
            return $this->db->update($this->table_name, $fields);
        }
    }

    function delete($id) {
        $this->db->where($this->_pk, $id);
        $update = array($this->active => 0);
        $this->db->update($this->table_name, $update);
        if ($this->db->affected_rows() != 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @tutorial get all by primary key
     * @param integer $id
     * @return	array $data
     */
    function get_by_pk($id) {
        $this->db->where($this->_pk, $id);
        $data = $this->db->get($this->table_name);
        return $data;
    }

    /**
     * @tutorial GET ALL ROWS
     * @param array $params
     * @param integer $limit
     * @param integer $offset
     * @return obj
     */
    function get($params = FALSE, $limit = 100000, $offset = 0, $active = FALSE, $order_by=FALSE) {
        //if do not declare $active then, always get active=1.
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($params) {
            $query = $this->db->get_where($this->table_name, $params, $limit, $offset);
        } else {
            $query = $this->db->get($this->table_name, $limit, $offset);
        }
        $data = $query->result();
        return $data;
    }
    /**
     * @tutorial GET ALL ROWS WITH LIKE STATEMENT
     * @param array $params
     * @param integer $limit
     * @param integer $offset
     * @return obj
     */
    function get_like($params = FALSE, $limit = 100000, $offset = 0, $active = FALSE, $like=1, $fields = FALSE) {
        //if do not declare $active then, always get active=1.
        if($fields){
            $this->db->select($fields);
        }
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        if ($params) {
            //$query = $this->db->get_where($this->table_name, $params, $limit, $offset);
            switch ($like){
                case 1:
                    $query = $this->db->like($params);
                    break;
                case 2:
                    $query = $this->db->or_like($params);
                    break;
                case 3:
                    $query = $this->db->not_like($params);
                    break;
                case 3:
                    $query = $this->db->or_not_like($params);
                    break;
            }
        } else {
            $query = $this->db->get($this->table_name, $limit, $offset);
        }
        $query = $this->db->get($this->table_name, $limit, $offset);
        $data = $query->result();
        return $data;
    }
    /**
     * @tutorial GET ALL ROWS WITH "WHERE IN" STATEMENT
     * @param array $params
     * @param integer $limit
     * @param integer $offset
     * @return obj
     */
    function get_in($params = FALSE, $limit = 100000, $offset = 0, $active = FALSE, $fields = FALSE, $params_in=false) {
        //if do not declare $active then, always get active=1.
        if($fields){
            $this->db->select($fields);
        }
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        if ($params || $params_in) {
            if ($params) {
                $query = $this->db->get_where($this->table_name, $params, $limit, $offset);
            }
            if ($params_in) {
                //$query = $this->db->get_where($this->table_name, $params, $limit, $offset);
                $query = $this->db->where_in(key($params_in), $params_in[key($params_in)]);
            }
        }else {
            $query = $this->db->get($this->table_name, $limit, $offset);
        }
        $query = $this->db->get($this->table_name, $limit, $offset);
        $data = $query->result();
        return $data;
    }
    /**
     * @tutorial GET ALL ROWS
     * @param array $params
     * @param integer $limit
     * @param integer $offset
     * @return obj
     */
    function get_array_number($params = FALSE, $limit = 100000, $offset = 0, $active = FALSE, $fields = FALSE) {
        //if do not declare $active then, always get active=1.
        if($fields){
            $this->db->select($fields);
        }
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        if ($params) {
            $query = $this->db->get_where($this->table_name, $params, $limit, $offset);
        } else {
            $query = $this->db->get($this->table_name, $limit, $offset);
        }
        $data = $query->result();

        $arr2 = array();
        foreach ($data as $row):
            $arr = array();
            foreach ($row as $value):
                $arr[] = $value;
            endforeach;
            $arr2[] = $arr;
        endforeach;

        return $arr2;
    }


    /**
     * @tutorial GET ALL ROWS
     * @param array $params
     * @param boolean $active
     * @return integer
     */
    function get_count($params = FALSE,$active = FALSE) {
        //if do not declare $active then, always get active=1
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        if ($params) {
            $data = $this->db->get_where($this->table_name, $params)->num_rows();
        } else {
            $data = $this->db->get($this->table_name)->num_rows();
        }
        return $data;
    }



    /**
     * @tutorial GET ONLY ONE ROW
     * @param array $params
     * @param boolean $active default FALSE
     * @return obj
     */
    function get_one($params, $active = FALSE) {
        //if do not declare $active then, always get active=0.
        if (!$active) {
            $this->db->where($this->active, 1);
        }
        $query = $this->db->get_where($this->table_name, $params, 1);
        return $query->row();
    }

    /**
     * @tutorial remove physically an row
     * @param id $id
     * @return boolean
     */
    function delete_physically($id) {
        $this->db->where($this->_pk,$id);
        $data = $this->db->delete($this->table_name);
        return $data;
    }

    /**
     * Logical delete using distinct field to primary key.
     * @param array key/value to where
     * @return boolean
     *
     */
    function delete_where($params){
        foreach($params as $key=>$value) {
            $this->db->where($key, $value);
        }

        $update = array($this->active => 0);
        if($this->db->update($this->table_name, $update) == FALSE) {
            if($error = $this->db->_error_message()){
                log_message('error', $error);
            }
            return FALSE;
        }else{
            return TRUE;
        }
    }

    /**
     * Update_pk
     * @param array key/value to where
     * @return boolean
     *
     */
    function update_pk($fields, $usr = NULL)
    {
        $pk = $this->_pk;

        $this->db->where($pk, $usr);
        return $this->db->update($this->table_name, $fields);
    }

    public function exists($params) {
        $query = $this->db->get_where($this->table_name, $params);
        $data = $query->result();
        if(count($data) > 0)
        {
            return true;
        } else {
            return false;
        }
    }


}