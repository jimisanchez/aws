<?php
	class Auth_model extends MY_Model {

		function __construct() {
			parent::__construct();
		}

		 function login($username, $password)	{

 			  $data = $this->db->select('id, email, password')
 			  			->from('users')
 			  			->where('email', $username)
 			  			->where('password', MD5($password))
 			  			->where('active',true)
 			  			->limit(1)
 			  			->get()->row();

  				return $data;

		 }

		 function is_loggedin() {
		 	if (empty($this->session->userdata['id'])) {
		 		return false;
		 	} else {
		 		return true;
		 	}
		 }
	}