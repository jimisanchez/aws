<?php

	foreach ($health as $key => $metric) {

		$data = $metric->toArray();
		echo '<b>' . $data['Label'] . '</b> ' .( empty($data['Datapoints']) ? 'No Data' : '') .' <br> ' ;

		foreach ( $data['Datapoints'] as $k => $val) {
			echo date('Y-m-d h:i a', strtotime($val['Timestamp'])) . '  ' . $val['Average'] . '<br>';
		}

		echo '<br>';

	}
?>