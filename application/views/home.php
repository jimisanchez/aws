    <?php $this->load->view('templates/header.php');?>
    <div class="container" >
    <nav class="navbar navbar-default" role="navigation" >
        <div class="container-fluid" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" >
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only" >Toggle navigation</span>
                    <span class="icon-bar" ></span>
                    <span class="icon-bar" ></span>
                    <span class="icon-bar" ></span>
                </button>
                <a class="navbar-brand" href="#" >CH</a>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
                <ul class="nav navbar-nav" >
                    
                    
                    <li class="dropdown" >
                        
                        <ul class="dropdown-menu" role="menu" >
                            <li ><a href="#" >Billing</a></li>
                            <li ><a href="#" >Administration</a></li>
                            <li ><a href="#" >Forgot Password</a></li>
                        </ul>
                    </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right" >
                    
                    <li class="dropdown" >
                        <a href="#" class="dropdown-toggle btn-default" data-toggle="dropdown" >Tools <b class="caret" ></b></a>
                        <ul class="dropdown-menu" role="menu" >
                            <li ><a href="/aws/main/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<div class="row" >
    <div class="col-md-16">
        <div class="panel panel-success" data-bind="visible: instances().length > 0">
            <div class="panel-heading" >Instances</div>
            <div class="panel-body" >
                <ul class="list-group" id="instances_list" data-bind="foreach { data: instances, as: 'instance' }, visible: instances().length > 0">
                    <li class="list-group-item">
                        <i class="fa fa-desktop fa-5x" data-bind="style: { color: instance.State.Code == 16 ? 'green' : ( instance.State.Code == 48 ? 'red' : 'black') }"></i>
                        <i class="fa fa-play" style="color:green" data-bind="click: $parent.startInstance, visible: instance.State.Code == 80"></i>
                        <i class="fa fa-stop" data-bind="click: $parent.stopInstance, visible: instance.State.Code == 16"></i>
                        <i class="fa fa-trash-o" data-bind="click: $parent.deleteInstance, visible: instance.State.Code != 48"></i>
                        <i class="fa fa-heart"  data-bind="click: $parent.healthCheck" style="color:red;"></i>
                        <i class="fa fa-info-circle" data-bind="click: $parent.information"></i>
                    </li>
                </ul>
                You have <b data-bind="text: instances().length">&nbsp;</b> instance(s)
    </div>
        <div class="panel-footer" ></div>
    </div>
    </div>
        <div class="col-md-4" >
    <div class="panel panel-primary" >
        <div class="panel-heading" >Choose Your OS!<span class="badge text-warning pull-right" >1</span></div>
        <div class="panel-body" ><img data-awsimageid='ami-76817c1e' class="img-circle bg-primary" src="<?php echo base_url();?>assets/images/linux_icon.png" id="linux_icon" ><img class="img-circle bg-primary" src="<?php echo base_url();?>assets/images/freebsd_icon.png" id='freebsd_icon' ></div>
        <div class="panel-footer" ></div>
    </div>
</div>
        <div class="col-md-4" >
    <div class="panel panel-warning" >
        <div class="panel-heading" >Configure It!<span class="badge pull-right" >2</span></div>
        <div class="panel-body" ><img data-awsimageid='ami-a6926dce' class="img-circle img-responsive text-center center-block bg-warning" src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Wrench_font_awesome.svg/120px-Wrench_font_awesome.svg.png" id="configure_drop" ></div>

        <form role="form" id="configure_form" style="display:none;" _lpchecked="1">
            <input class="form-control" id="instance_name" placeholder="Instance Name">
            <select class="form-control" >
                <option>5G Ram</option>
                <option>15G Ram</option>
                <option>25G Ram</option>

                </select>
        </form>

        <div class="panel-footer" ></div>
    </div>
</div>
        <div class="col-md-4" >
    <div class="panel panel-success" >
        <div class="panel-heading" >Build It!<span class="badge pull-right" >3</span></div>
        <div class="panel-body" >
            <button type="button" class="btn btn-success btn-lg center-block" id="build_button">Build</button>
            <img style="display:none;"  class="center-block" id="building_gear" src="<?php echo base_url();?>assets/images/building_gear.gif" />
            <i class="fa fa-check fa-3x" id="build_complete" style="display:none"></i>
        </div>
        <div class="panel-footer" ></div>
    </div>
</div>
    </div></div>


    <div  style="display:none;" id="selectedInstance" title="Instance information" data-bind="with: selected_instance">
        
        Architecture : <span data-bind="text: Hypervisor"> </span> <br>
        Hypervisor : <span data-bind="text: Hypervisor" > </span> <br>
        ImageId : <span data-bind="text: ImageId" > </span> <br>
        ImageId : <span data-bind="text: ImageId" > </span> <br>
        LaunchTime : <span data-bind="text: LaunchTime" > </span> <br>
        State : <span data-bind="text: State.Name" > </span> <br>
        PublicDnsName: <span data-bind="text: PublicDnsName" > </span> <br>
        RootDeviceName: <span data-bind="text: RootDeviceName" > </span> <br>
        RootDeviceType: <span data-bind="text: RootDeviceType" > </span> <br>

 
    </div>


    <input type="hidden" id="selected_image_id" />
    <?php $this->load->view('templates/footer'); ?>
