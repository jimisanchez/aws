    <?php $this->load->view('templates/header.php');?>
    <div class="container" >
    <nav class="navbar navbar-default" role="navigation" >
        <div class="container-fluid" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" >
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only" >Toggle navigation</span>
                    <span class="icon-bar" ></span>
                    <span class="icon-bar" ></span>
                    <span class="icon-bar" ></span>
                </button>
                <a class="navbar-brand" href="#" >CH</a>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
                <ul class="nav navbar-nav" >
                    
                    
                    <li class="dropdown" >
                        
                        <ul class="dropdown-menu" role="menu" >
                            <li ><a href="#" >Billing</a></li>
                            <li ><a href="#" >Administration</a></li>
                            <li ><a href="#" >Forgot Password</a></li>
                        </ul>
                    </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right" >
                    
                    <li class="dropdown" >
                        <a href="#" class="dropdown-toggle btn-default" data-toggle="dropdown" >Tools <b class="caret" ></b></a>
                        <ul class="dropdown-menu" role="menu" >
                            <li ><a href="#" >Billing</a></li>
                            <li ><a href="#" >Administration</a></li>
                            <li ><a href="#" >Forgot Password</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<div class="jumbotron" >
        <h1 >Login</h1>
        
    <form class="form-horizontal" role="form"  _lpchecked="1">
        <div class="form-group" >
            <label for="inputEmail3" class="col-sm-2 control-label" >Email</label>
            <div class="col-sm-10" >
                <input type="email" class="form-control" id="username" placeholder="Email"  autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDhss3LcOZQAAAU5JREFUOMvdkzFLA0EQhd/bO7iIYmklaCUopLAQA6KNaawt9BeIgnUwLHPJRchfEBR7CyGWgiDY2SlIQBT/gDaCoGDudiy8SLwkBiwz1c7y+GZ25i0wnFEqlSZFZKGdi8iiiOR7aU32QkR2c7ncPcljAARAkgckb8IwrGf1fg/oJ8lRAHkR2VDVmOQ8AKjqY1bMHgCGYXhFchnAg6omJGcBXEZRtNoXYK2dMsaMt1qtD9/3p40x5yS9tHICYF1Vn0mOxXH8Uq/Xb389wff9PQDbQRB0t/QNOiPZ1h4B2MoO0fxnYz8dOOcOVbWhqq8kJzzPa3RAXZIkawCenHMjJN/+GiIqlcoFgKKq3pEMAMwAuCa5VK1W3SAfbAIopum+cy5KzwXn3M5AI6XVYlVt1mq1U8/zTlS1CeC9j2+6o1wuz1lrVzpWXLDWTg3pz/0CQnd2Jos49xUAAAAASUVORK5CYII=); background-attachment: scroll; cursor: auto; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
            </div>
        </div>
        <div class="form-group" >
            <label for="inputPassword3" class="col-sm-2 control-label" >Password</label>
            <div class="col-sm-10" >
                <input type="password" class="form-control" id="password" placeholder="Password"  autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDhss3LcOZQAAAU5JREFUOMvdkzFLA0EQhd/bO7iIYmklaCUopLAQA6KNaawt9BeIgnUwLHPJRchfEBR7CyGWgiDY2SlIQBT/gDaCoGDudiy8SLwkBiwz1c7y+GZ25i0wnFEqlSZFZKGdi8iiiOR7aU32QkR2c7ncPcljAARAkgckb8IwrGf1fg/oJ8lRAHkR2VDVmOQ8AKjqY1bMHgCGYXhFchnAg6omJGcBXEZRtNoXYK2dMsaMt1qtD9/3p40x5yS9tHICYF1Vn0mOxXH8Uq/Xb389wff9PQDbQRB0t/QNOiPZ1h4B2MoO0fxnYz8dOOcOVbWhqq8kJzzPa3RAXZIkawCenHMjJN/+GiIqlcoFgKKq3pEMAMwAuCa5VK1W3SAfbAIopum+cy5KzwXn3M5AI6XVYlVt1mq1U8/zTlS1CeC9j2+6o1wuz1lrVzpWXLDWTg3pz/0CQnd2Jos49xUAAAAASUVORK5CYII=); background-attachment: scroll; cursor: auto; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-offset-2 col-sm-10" >
                <div class="checkbox" >
                    <label >
                        <input type="checkbox" > Remember me
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-offset-2 col-sm-10" >
                <button type="button" class="btn btn-primary"  id="login_button">Sign in</button>
            </div>
        </div>
    </form>

    
    <div class="progress" >
        <div class="progress-bar progress-bar-success visible-lg" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" id="progress_bar" style="display:none;"></div>
    </div>
</div></div>
    <?php $this->load->view('templates/footer'); ?>