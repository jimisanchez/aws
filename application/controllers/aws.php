<?php

	class AWS extends CI_Controller {

		private $_credentials;
		protected  $_config_array;

		function __construct() {
			parent::__construct();
			$this->_credentials  = new Aws\Common\Credentials\Credentials('AKIAIJ4GILSUQA2V74FA', 'TY1ZSKRSxMns+o7lr7AXEmR0Oetl3UQVxqmu4SRJ');
			$this->_config_array =  array(
												'profile' => 'default',
												'region'  => 'us-east-1',
												'credentials' => $this->_credentials,
												);
		}

		function sleep() {
			sleep(3);
			echo json_encode(array());
		}

		function createInstance() {
			
			$image = $this->input->post('image');
			switch ( strtolower($image)) {
			    default:
				case 'linux_incon':
					$imageId = 'ami-785bae10';
					break;
			    case 'freebsd_icon':
			    	$imageId = 'ami-76817c1e';
			    	break;
			}
			if ( empty($image) ) {
				return false;
			}

			$client = Aws\Ec2\Ec2Client::factory($this->_config_array);
			// Launch an instance with the key pair and security group
			$result = $client->runInstances(
											array(
													'ImageId' => $imageId,
													'MinCount'=> 1,
													'MaxCount' => 1,
													'InstanceType' => 't2.micro')	
												);
			$instanceIds = $result->getPath('Instances/*/InstanceId');

			echo json_encode($instanceIds);
		}

		function deleteInstance() {
			$instance_id = $this->input->post('instance_id');

			$client = Aws\Ec2\Ec2Client::factory($this->_config_array);
			try {
				$result = $client->terminateInstances(
								array(
							    'DryRun' => false,
							    'InstanceIds' => array($instance_id),
							));
				echo json_encode($result);
			} catch (Exception $e) {
				echo json_encode(array($e->getMessage()));
			}

		}

		function getMyInstances() {

			$client = Aws\Ec2\Ec2Client::factory($this->_config_array);

			$instances_array = array();

			try {


				$data = $client->describeInstances();

				if ( !empty($data) ) {

					if ( !empty($data['Reservations']) && is_array($data['Reservations']) ) {
						
						foreach ($data['Reservations'] as $key => $reservation) {

							if ( is_array($reservation['Instances'])) {
						
								foreach ($reservation['Instances'] as $reservation_key => $instance_array ) {
										$instances_array[] = $instance_array;
						
								}
						
							}
						}
					}
				}
			} catch (Exception $exception) {
				return $instances_array;
			}

			echo json_encode($instances_array);
		}

		function stopInstance() {
			$client = Aws\Ec2\Ec2Client::factory($this->_config_array);
			$instance_id = $this->input->post('instance_id');
			try {
				$data = $client->stopInstances( array('DryRun' => false, 'Force' => true, 'InstanceIds' => array($instance_id) ) );
				echo json_encode($data);
			} catch (Exception $exception) {
				echo json_encode( array($exception->getMessage()) ) ;
			}
		}

		function startInstance() {
			$client = Aws\Ec2\Ec2Client::factory($this->_config_array);
			$instance_id = $this->input->post('instance_id');


			try {
					$result = $client->startInstances(
														array(
															    'InstanceIds' => array($instance_id),
															    'DryRun' =>false)
														);
					echo json_encode($result);
			} catch (Exception $e) {
				echo json_encode( array($e->getMessage()));
			}


		}

		function getCloudStatisticsByInstanceId($instance_id) {
			$metrics = $this->getCloudMetrics();
			$return_array = array();
			foreach ($metrics['Metrics'] as $metric) {
				foreach ($metric['Dimensions'] as $dimension) {
					$data_request = array(
							'Namespace' => $metric['Namespace'],
							'MetricName' => $metric['MetricName'],
							'Dimensions' => array(array('Name' => 'InstanceId', 'Value' => $instance_id)),
							'StartTime' => strtotime( date('Y-m-d 00:00:00', strtotime('-1 day')) ),
							'EndTime' => strtotime( date('Y-m-d 23:59:59') ),
							'Period' => 600,
							'Statistics' => array('Average'));
					$return_array[] = $this->getStatistics($data_request);
				}
			}
			return $return_array;
		}

		function getCloudMetrics() {
			$client = Aws\CloudWatch\CloudWatchClient::factory($this->_config_array);
			$result = $metrics = $client->listMetrics(array('Namespace' => 'AWS/EC2'));

			return $result;

		}

		function getStatistics($data_request) {
			$client = Aws\CloudWatch\CloudWatchClient::factory($this->_config_array);
			$data = $client->getMetricStatistics($data_request);
			return $data;
		}
	
	}