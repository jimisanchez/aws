<?php

	class Main extends CI_Controller {


		function __construct() {
			parent::__construct();
			$this->load->model('auth/auth_model','auth');
		}

		function index() {
			$this->load->view('index');
		}

		function home() {
			session_start();
			$this->check_loggedin();
			$this->load->view('home');
		}

		function healthcheck() {
			$this->load->library('../controllers/aws');
			$instance_id = $this->uri->segment(3);
			$data['health'] = $this->aws->getCloudStatisticsByInstanceId($instance_id);

			$this->load->view('healthcheck', $data);
		}

		function login() {
			$post_data = $this->input->post(null);
			
			$data = $this->auth->login($post_data['username'],$post_data['password']);

			if ( isset($data->id) && !empty($data->id) ) {
				$this->session->set_userdata($data);
			}
			echo json_encode($data);
		}

		function logout() {
			session_start();
			$this->session->sess_destroy();
			redirect('/main/');

		}

		function check_loggedin() {
			if ( ! $this->auth->is_loggedin() ) {
				redirect('/main/');
			}
		}
		
	}