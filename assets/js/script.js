$(document).ready( function() {
	
	$('#login_button').on('click', function() {
		username = $('#username').val();
		password = $('#password').val();

			$.ajax({
			  type: "POST",
			  url: "/aws/main/login",
			  data: { username: username, password: password },
			  dataType : "json"
			}).done(function( user ) {
			    console.dir(user);
			    if ( user.id ) {
			    	console.log('sucess');
			    	successLogin();
			    } else {
			    	console.log('failure');
			    	failLogin();
			    }
			  });

	});


	$('#build_button').on('click', function(){
		if ( $('#configure_form').is(':hidden') ) {
			$('.img-circle').effect( "shake" );
		} else {
			if ( $('#instance_name').val() == "") {
				$('#instance_name').effect('shake');
			} else {
				$('#build_button').hide();
				$('#building_gear').show();


			$.ajax({
			  type: "POST",
			  url: "/aws/aws/createInstance",
			  data : { image : $('#selected_image_id').val() },
			  dataType : "json"
			}).done(function( data ) {

				$('#building_gear').hide();
				$('#build_complete').show();

			  });

			}
		}
	});


	$('.img-circle').draggable({
									       opacity: 0.70,
									       zIndex:10000,
									       appendTo: "body"
	   });

	if ( $('#configure_drop').length ) {
		$('#configure_drop').droppable({
									      drop: function( event, ui ) {
									        $( this ).hide();
									        $('#selected_image_id').val( $(ui.draggable).attr('id') );
									        $(ui.draggable).remove();
									        $('#configure_form').show();
									        $('.img-circle').draggable().draggable( "disable" );
									      }
    });
	}

	getInstances();
	setInterval( function() { getInstances();}, 10000);
	ko.applyBindings(AppViewModel);


});


	function successLogin() {

		$('#progress_bar').width('25%').removeClass('progress-bar-danger').addClass('progress-bar-success')
		
		setTimeout( function() { $('#progress_bar').width('25%') }, 500 );
		setTimeout( function() { $('#progress_bar').width('50%') }, 500 );
		setTimeout( function() { $('#progress_bar').width('75%') }, 500 );
		setTimeout( function() { $('#progress_bar').width('100%') }, 500 );

		$('#login_button').addClass('btn-success').removeClass('btn-danger');

		setTimeout( function() { window.location = '/aws/main/home'; }, 2000);

	}

	function failLogin() {

		$('#login_button').addClass('btn-danger').removeClass('btn-success');
		setTimeout( function() { $('#progress_bar').width('100%').removeClass('progress-bar-success').addClass('progress-bar-danger') }, 500 );

	}

	function getInstances() {
		$.ajax({
			type: "POST",
			url: "/aws/aws/getMyInstances",
			dataType : "json"
		}).done(function( data ) {
			AppViewModel.instances.removeAll();
			$.each(data, function(i, obj) {
				console.dir(obj);
			AppViewModel.instances.push( obj );
			});
		});
	}

	var AppViewModel = {
    		instances : ko.observableArray([]),
    		selected_instance : ko.observable()

	}

	AppViewModel.deleteInstance = function(instance) {
		$('<div title="Confirm Delete">Do you really want to delete the instance with id <b>'+instance.InstanceId+'</b></div>').dialog({
  dialogClass: "no-close",
  buttons: [
    {
      text: "Delete",
      class : "btn-danger",
      click: function() {
        $( this ).dialog( "close" );
       		$.ajax({
			type: "POST",
			url: "/aws/aws/deleteInstance",
			dataType : "json",
			data : { instance_id : instance.InstanceId }
		}).done( function(data) {
			getInstances();
		}); 
      }
    },
    {
    	text : "Cancel",
    	class : "btn-success",
    	click : function() {
    		$(this).dialog("close");
    	}
    }
  ]
});
	};

	AppViewModel.startInstance = function(instance) {
		$.ajax(

				{
					type : "POST",
					url : "/aws/aws/startInstance",
					dataType : "json",
					data : { instance_id : instance.InstanceId }
				}
			).done( function(data) {
				getInstances();
			});
	}

	AppViewModel.stopInstance = function(instance) {
				$.ajax(

				{
					type : "POST",
					url : "/aws/aws/stopInstance",
					dataType : "json",
					data : { instance_id : instance.InstanceId }
				}
			).done( function(data) {
				getInstances();
			});
	}

	AppViewModel.healthCheck = function(instance) {

		var url = '/aws/main/healthcheck/' + instance.InstanceId;
		var windowName = "health check";
		var windowSize = 1;

		window.open(url, windowName, 1);

		event.preventDefault();
	}

	AppViewModel.information = function(instance) {

		AppViewModel.selected_instance(instance);
		$('#selectedInstance').dialog( { width:'auto' } );
 
	}